<?php

class Player {
    private string $gender;
    private string $name;
    private int $height;
    private DateTime $birth;

    public function getGender() {
        return $this->gender;
    }

    public function setGender(string $gender) {
        if (in_array($gender, ['female', 'male'])){
            $this->gender = $gender;
        }
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) { // interdit null mais permet ''
        if (!empty($name)) { // interdit ''
            $this->name = $name;
        }
    }

    public function getHeight() {
        return $this->height;
    }

    public function setHeight(int $height) {
        if ($height > 0) {
            $this->height = $height;
        }
    }

    public function getBirth() {
        return $this->birth;
    }

    public function setBirth(DateTime $birth) {
        if ($birth <= new DateTime) {
            $this->birth = $birth;
        }
    }

    public function getAge() {
        $date = new DateTime();
        $birthDate = $this->getBirth();
        $ageInterval = $date->diff($birthDate);

        return $ageInterval->y;
    }

    public function equals(Player $player) {
        if (
            $this->name === $player->getName()
            &&
            $this->birth === $player->getBirth()
        ) {
            return true;
        }

        return false;
    }
}