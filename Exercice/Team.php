<?php

class Team {
    private string $name;
    private string $city;
    private array $players = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getPlayers() {
        return $this->players;
    }

    public function addPlayer(Player $player) {
        $present = false;

        foreach($this->players as $teamMember) {
            if($teamMember->equals($player)) {
                $present = true;
            }
        }

        if (!$present) {
            $this->players[] = $player;
        }
    }

    public function removePlayer(Player $player) {
        foreach ($this->players as $key => $teamMember) {
            if ($teamMember->equals($player)) {
                unset($this->players[$key]);
            }
        }
    }

    public function getSize() {
        return count($this->players);
    }
}