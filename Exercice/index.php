<?php
require_once('Player.php');
require_once('Team.php');

$player1 = new Player();
$player1->setName('John');
$player1->setGender('male');
$player1->setHeight(183);
$dateOfBirth = new DateTime('2002-10-03');
$player1->setBirth($dateOfBirth);

echo ('Le nom du joueur 1 est '.$player1->getName()."\n");

$player2 = new Player();
$player2->setName('Jane');
$player2->setGender('female');
$player2->setHeight(172);
$dateOfBirth = new DateTime('2000-02-21');
$player2->setBirth($dateOfBirth);



echo ('Le nom du joueur 2 est '.$player2->getName()."\n");
echo ('Lage du joueur 2 est '.$player2->getAge()."\n");


$team = new Team();

$team->setName('Union Bordeaux Bègle');
$team->setCity('Bordeaux');

echo ('Taille de l\'équipe '.$team->getName().' de '.$team->getCity().' : '.$team->getSize(). ' joueur(s)'."\n");

$team->addPlayer($player1);
$team->addPlayer($player2);

echo ('Taille de l\'équipe '.$team->getName().' de '.$team->getCity().' : '.$team->getSize(). ' joueur(s)'."\n");

$team->removePlayer($player1);

echo ('Taille de l\'équipe '.$team->getName().' de '.$team->getCity().' : '.$team->getSize(). ' joueur(s)'."\n");
