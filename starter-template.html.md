# Code html pour une page de démarrage avec [Bootstrap](https://getbootstrap.com)

```php
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Première application Symfony</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
          <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
            <span class="fs-4">Symfony - To Do list</span>
          </a>

          <ul class="nav nav-pills">
            <li class="nav-item"><a href="#" class="nav-link active" aria-current="page">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Menu 1</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Menu 2</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Menu 2</a></li>
            <li class="nav-item"><a href="#" class="nav-link">A propos</a></li>
          </ul>
        </header>
      </div>
      <div class="b-example-divider"></div>
    <div class="col-lg-8 mx-auto p-4 py-md-5">
      <main>
        <h1>Page de démarrage</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut arcu ipsum, interdum sit amet dolor non, tincidunt maximus metus. Sed sit amet tortor in ipsum volutpat vulputate sit amet vitae diam. Phasellus bibendum quam et metus bibendum, sed eleifend metus convallis. Donec ligula lacus, congue interdum ante a, semper tincidunt magna. Sed ligula enim, gravida convallis justo ut, suscipit luctus nunc. Pellentesque eleifend nibh eu est sodales aliquet. Nunc sit amet venenatis lacus, at euismod sapien. Praesent sit amet mattis elit, in congue sem. Vivamus sodales risus a lacinia interdum. Ut eu enim luctus, condimentum tortor at, scelerisque dui. Etiam molestie turpis sed nibh aliquam, ac lacinia tortor lacinia. Pellentesque sit amet vestibulum mauris. Fusce egestas ac ante sit amet gravida. Praesent tellus arcu, pulvinar vel sollicitudin ac, convallis sit amet mauris. Sed at facilisis arcu, nec elementum tellus.</p>
        <p>Nullam et consectetur metus, non lacinia libero. Praesent blandit tincidunt velit non placerat. Etiam tempus ipsum ornare quam pharetra lobortis. Ut mattis pellentesque dui. Sed id consequat nisi. Nunc quis justo hendrerit, rhoncus diam nec, iaculis ipsum. Morbi nibh eros, dictum ac erat eu, venenatis scelerisque purus. Nulla quis erat vel dolor aliquet finibus ac maximus odio. Nulla ullamcorper dui libero, porta accumsan massa rhoncus id.</p>

        <div class="mb-5">
          <a href="" class="btn btn-primary btn-lg px-4">Bouton</a>
        </div>

        <div class="row">
          <div class="col-md-6">
            <h2>Première colonne</h2>
            <p>Nulla quis fringilla nulla, rutrum mollis tortor. Nam ultrices sodales rhoncus. Duis interdum ullamcorper enim, in porta orci suscipit at. Phasellus pharetra quam interdum turpis eleifend rutrum sit amet et ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed a quam a justo auctor eleifend non vitae enim. Maecenas varius vel diam id consequat.</p>
          </div>

          <div class="col-md-6">
            <h2>Deuxième colonne</h2>
            <p>Nullam eget condimentum metus. Donec venenatis luctus nibh, nec pellentesque turpis tincidunt eu. Donec porta gravida massa sollicitudin mattis. Curabitur sed sapien non nisi gravida porttitor ac vel dolor. Praesent a tellus ac libero viverra ornare. Nunc mollis nunc ac erat vestibulum, vitae tristique sapien elementum. Nulla libero ante, porta vitae porta nec, imperdiet quis arcu. Sed sit amet tempor magna. Quisque at lacus blandit, varius sapien in, rutrum lacus. Aliquam id dolor nisl. Pellentesque ut lorem vitae diam aliquam egestas.</p>
          </div>
        </div>
      </main>
      <footer class="pt-5 my-5 text-muted border-top">
        Made in Bordeaux · 2022
      </footer>
    </div>

    <script src="/docs/5.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>
</html>

```