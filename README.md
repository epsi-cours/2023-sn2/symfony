# EPSI - SN2 - Cours sur Symfony

Définir et créer une application de bloc note à l'aide du framework Symfony 6

## Rappels techniques
### Le modèle MVC

https://fr.wikipedia.org/wiki/Modèle-vue-contrôleur

![image](https://upload.wikimedia.org/wikipedia/commons/b/b2/Modèle-vue-contrôleur_%28MVC%29_-_fr.png)

- [Classe Joueur réalisée en cours](ExerciceCalulatrice/Calculatrice.php)
- [Mini controller executant la classe Calculatrice](ExerciceCalulatrice/index.php)

### Découverte ou rappel sur le modèle orienté objet

https://fr.wikipedia.org/wiki/Programmation_orientée_objet

## Installation de Symfony

### Pré requis :
- Avoir PHP installé sur son poste en version 8.0 ou supérieure
- Avoir une base de données MySQL installée sur son poste

Sur windows : XAMPP Server peut être une solution pour ces deux points

https://www.apachefriends.org/fr/download.html

Sur Mac :

https://brew.sh
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew install php
brew install mysql
```

Editeur de code :
- Visual studio code, open source : https://code.visualstudio.com/download
- PHPStorm, gratuit pendant 30 jours : https://www.jetbrains.com/fr-fr/phpstorm/download


### Installer Composer

https://getcomposer.org/download/
```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

sudo mv composer.phar /usr/local/bin/composer
```

### Installer Symfony CLI
https://symfony.com/download

MacOS
```bash
brew install symfony-cli/tap/symfony-cli
```

Windows
```bash
scoop install symfony-cli
```

Linux
```bash
curl -sS https://get.symfony.com/cli/installer | bash
```

### Créer le projet Symfony

Projet Web
```bash
symfony new my_project --webapp
composer install
```

### Démarrage d'un serveur web

```bash
symfony server:list # Lister les serveurs actuellement démarrés
symfony server:start # Démarrer un serveur web ayant le dossier actuel comme repertoire de base (document root)
symfony server:start -d # Démarrer un serveur web ayant le dossier actuel comme repertoire de base (document root) en mode daemon
symfony server:stop # Stopper le serveur démarré ayant le dossier actuel comme document root
```

### Etude de l'architecture d'un projet Symfony vierge

```
/bin
    /console
/config
    /services.yaml
/public
    /index.php
/src
    /Controller
    /Entity
    /Form
    /Repository
/templates
/tests
/translations
    /messages.fr.yaml
/var
/vendor
.env
.env.local
.gitignore
composer.json
composer.lock
```

## Développement d'une première application

L'ensemble de l'exercice To Do List que nous réalisons pendant le cours, sont poussés sur un second repository situé ici :
[Exercice de To Do List - Projet Symfony](https://gitlab.com/epsi-cours/2023-sn2/symfony-to-do-list)

### Premier controllers, premieres vues

Création du controller de la page d'accueil
```bash
bin/console make:controller
```
Nommer le controller HomeController

Création du template général
[Code HTML de démarrage](starter-template.html.md)

Création du template de la page d'accueil
Créer le template de page d'accueil à l'aide du code HTML fournis, à répartir dans les template `templates/base.html.twig` et `templates/home/index.html.twig`

Création du controller et du template de la page à propos
```bash
bin/console make:controller
```
Nommer le controller AboutController

Modifier la vue `templates/about/index.html.twig` pour contenir le cœur de la page à propos, en héritant du cadre global inclus dans le fichier `templates/base.html`


### Relation avec la base de données

Configuration de la connexion à la base de donées

Fichier .env
```env
DATABASE_URL=mysql://login:password@host:port/db_name?serverVersion=5.7
```

Création de la base
```bash
bin/console doctrine:database:create
```

Création du schéma
```bash
bin/console doctrine:schema:create
```

Mise à jour du schéma et principe de migrations
```bash
bin/console make:migration
bin/console doctrine:migrations:migrate
```

### Première entité

Création d'une entité `Task`

Présentation du composant maker
```bash
composer require --dev symfony/maker-bundle
composer install
```

```bash
bin/console make:entity
```

### Controller en lien avec une entité

Création d'une page qui liste les tâches


Création d'un formulaire permettant de créer une nouvelle tâche
- Etude des formulaires symfony
- Principe de token CSRF

Création d'une route permettant de supprimer une tâche


Création d'une route permettant de modifier une tâche
- Permettre de marquer une tâche comme effectuée

## Web Debug Toolbar

Etude des elements les plus importe de la web debug toolbar

## Tests

Principe de test unitaire
Principe de test fonctionnel

Création des tests unitaire de l'entité tâche
Création de tests fonctionnels pour tester les premières route déjà crées
- Page d'accueil
- Page A propos
- Page de listing des tâches
- Formulaire de création de tâche
- Bouton de suppression de tâche


## Authentification

Rappels généraux sur le principe d'authentification
- Cryptage / Décryptage
- Hashage

Sessions

Roles / droits

[Documentation sur la sécurité Symfony](https://symfony.com/doc/current/security.html)

## Pour aller plus loin
### Création d'un système d'authentification
Si le temps nous le permet, nous verrons la mise en place d'un système d'authentification de manière pouvoir attribuer les tâches à un utilisateur

Puis nous pourrons réaliser les améliorations suivantes :
- Créer un lien entre une tâche et un utilisateur pour que chaque utilisateur ne voit que les tâches qui lui appartiennent
- Ajouter sur le tableau de bord un indicateur permettant de compter le nombre total de tâches, le nombre de tâches déjà effectuées, celles qui restent à faire
- Créer un utilisateur administrateur qui puisse voir l'ensemble des tâches de chaque utilisateur
- Créer un tableau de bord pour l'administrateur permettant de voir en un coup d'œil le taux de complétion des tâches

## Ressources

- [Site de Symfony](https://symfony.com)
- [Documentation de Symfony](https://symfony.com/doc/current/index.html)
- [Installation](https://symfony.com/doc/current/setup.html)
- [Symfony Casts](https://symfonycasts.com)
- [Cours Symfony sur Open Classrooms](https://openclassrooms.com/fr/courses/5489656-construisez-un-site-web-a-l-aide-du-framework-symfony-5) encore valable pour Symfony 6
- [L'importance de la qualité du code](https://openclassrooms.com/fr/courses/5489656-construisez-un-site-web-a-l-aide-du-framework-symfony-5/5517036-qu-est-ce-qu-un-code-de-qualite)
